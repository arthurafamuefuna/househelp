﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HouseHelp.Data;
using HouseHelp.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace HouseHelp.Controllers
{
    [Authorize]
    public class EmployerController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public EmployerController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Employer
        [Authorize(Roles = ("Admin"))]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Employer.ToListAsync());
        }

        // GET: Employer/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employerModel = await _context.Employer
                .SingleOrDefaultAsync(m => m.EmployerId == id);
            if (employerModel == null)
            {
                return NotFound();
            }

            return View(employerModel);
        }

        // GET: Employer/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Employer/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = (""+AppUtils.AppRoles.EMPLOYER + "," + AppUtils.AppRoles.ADMIN+""))]
        public async Task<IActionResult> Create([Bind("EmployerId,Surname,Firstname,Lastname,Gender,Religion,Occupation," +
            "Address,State,Phone,Email,CreatedAt,UpdatedAt")] EmployerModel employerModel)
        {
            if (ModelState.IsValid)
            {
                var userEmail = _userManager.GetUserName(User).ToLower();
                employerModel.Email = userEmail;
                _context.Add(employerModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(HomeController.Index),"Home");
            }
            return View(employerModel);
        }

        // GET: Employer/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var employerModel = await _context.Employer.SingleOrDefaultAsync(m => m.EmployerId == id);
            if (employerModel == null)
            {
                return NotFound();
            }
            return View(employerModel);
        }

        // POST: Employer/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = ("" + AppUtils.AppRoles.EMPLOYER + "," + AppUtils.AppRoles.ADMIN + ""))]
        public async Task<IActionResult> Edit(int id, [Bind("EmployerId,Surname,Firstname,Lastname,Gender,Religion,Occupation," +
            "Address,State,Phone,Email,CreatedAt,UpdatedAt")] EmployerModel employerModel)
        {
            if (id != employerModel.EmployerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    employerModel.Email = _userManager.GetUserName(User);
                    _context.Update(employerModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployerModelExists(employerModel.EmployerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            return View(employerModel);
        }

        // GET: Employer/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employerModel = await _context.Employer
                .SingleOrDefaultAsync(m => m.EmployerId == id);
            if (employerModel == null)
            {
                return NotFound();
            }

            return View(employerModel);
        }

        // POST: Employer/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employerModel = await _context.Employer.SingleOrDefaultAsync(m => m.EmployerId == id);
            _context.Employer.Remove(employerModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmployerModelExists(int id)
        {
            return _context.Employer.Any(e => e.EmployerId == id);
        }
    }
}
