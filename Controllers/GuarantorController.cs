﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HouseHelp.Data;
using HouseHelp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace HouseHelp.Controllers
{
    [Authorize]
    public class GuarantorController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public GuarantorController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Guarantor
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Guarantor.ToListAsync());
        }

        // GET: Guarantor/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guarantorModel = await _context.Guarantor
                .SingleOrDefaultAsync(m => m.GuarantorId == id);
            if (guarantorModel == null)
            {
                return NotFound();
            }

            return View(guarantorModel);
        }

        // GET: Guarantor/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Guarantor/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GuarantorId,Surname,Firstname,Lastname,Gender,Religion,Address,State,LGA,Phone,Email,CreatedAt,UpdatedAt")] GuarantorModel guarantorModel)
        {
            if (ModelState.IsValid)
            {
                guarantorModel.Email = _userManager.GetUserId(User);
                _context.Add(guarantorModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            return View(guarantorModel);
        }

        // GET: Guarantor/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guarantorModel = await _context.Guarantor.SingleOrDefaultAsync(m => m.GuarantorId == id);
            if (guarantorModel == null)
            {
                return NotFound();
            }
            return View(guarantorModel);
        }

        // POST: Guarantor/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("GuarantorId,Surname,Firstname,Lastname,Gender,Religion,Address,State,LGA,Phone,Email,CreatedAt,UpdatedAt")] GuarantorModel guarantorModel)
        {
            if (id != guarantorModel.GuarantorId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    guarantorModel.Email = _userManager.GetUserName(User);
                    guarantorModel.UpdatedAt = DateTime.Now;
                    _context.Update(guarantorModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!GuarantorModelExists(guarantorModel.GuarantorId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            return View(guarantorModel);
        }

        // GET: Guarantor/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var guarantorModel = await _context.Guarantor
                .SingleOrDefaultAsync(m => m.GuarantorId == id);
            if (guarantorModel == null)
            {
                return NotFound();
            }

            return View(guarantorModel);
        }

        // POST: Guarantor/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var guarantorModel = await _context.Guarantor.SingleOrDefaultAsync(m => m.GuarantorId == id);
            _context.Guarantor.Remove(guarantorModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool GuarantorModelExists(int id)
        {
            return _context.Guarantor.Any(e => e.GuarantorId == id);
        }
    }
}
