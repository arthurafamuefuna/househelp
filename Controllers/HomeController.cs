﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HouseHelp.Models;
using HouseHelp.Data;
using Microsoft.AspNetCore.Identity;

namespace HouseHelp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public HomeController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var userEmail = _userManager.GetUserName(User).ToLower();
                if (User.IsInRole("Employer")) {

                    var employerId = _context.Employer.SingleOrDefault(c => c.Email.Equals(userEmail)).EmployerId;
                    ViewData["EmployerId"] = employerId; 
                }
                else if (User.IsInRole("Guarantor"))
                {
                    var guarantorId = _context.Guarantor.SingleOrDefault(c => c.Email.Equals(userEmail)).GuarantorId;
                    ViewData["GuarantorId"] = guarantorId;
                }
            }
            return View(await _context.Helper.ToListAsync());
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
