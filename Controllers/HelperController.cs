﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HouseHelp.Data;
using HouseHelp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace HouseHelp.Controllers
{
    [Authorize]
    public class HelperController : Controller
    {
        private readonly ApplicationDbContext _context;
        private UserManager<ApplicationUser> _userManager;

        public HelperController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Helper
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var userId = _userManager.GetUserId(User);
            var value = await _context.Helper.Where(c => c.ProviderId.Equals(userId)).ToListAsync();

            if (User.IsInRole(AppUtils.AppRoles.ADMIN)) return View(await _context.Helper.ToListAsync());

            return View(await _context.Helper.Where(c => c.ProviderId.Equals(userId)).ToListAsync());
        }

        // GET: Helper/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            {
            if (id == null)
                return NotFound();
            }

            var helperModel = await _context.Helper
                .SingleOrDefaultAsync(m => m.HelperId == id);
            if (helperModel == null)
            {
                return NotFound();
            }

            return View(helperModel);
        }

        public async Task<IActionResult> AddToList(int? id)
        {
            {
                if (id == null)
                    return NotFound();
            }

            var helperModel = await _context.Helper
                .SingleOrDefaultAsync(m => m.HelperId == id);
            if (helperModel == null)
            {
                return NotFound();
            }

            //var changeStatus = new HelperModel() { };
            helperModel.Status = AppUtils.HelperStatusEnum.Held;
            helperModel.Employer = _userManager.GetUserId(User);
            _context.Entry(helperModel).State = EntityState.Modified;
            _context.SaveChanges();
            
           

            return RedirectToAction("Index");
        }

        // GET: Helper/Create
        public IActionResult Create()
        {
            return View();
        }

        public async Task<IActionResult> Checklist()
        {
            string userId = _userManager.GetUserId(User);

            return View(await _context.Helper.Where(c => c.Employer.Equals(userId)).ToListAsync());
        }

        // POST: Helper/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles =("Admin, Guarantor"))]
        public async Task<IActionResult> Create([Bind("HelperId,Surname,Firstname,Lastname,Gender,State,Religion," +
            "Qualification,Language,DateOfBirth,NextOfKinName,NextOfKinAddress,NextOfKinPhone,ProviderId,CreatedAt,UpdatedAt")] HelperModel helperModel)
        {
            if (ModelState.IsValid)
            {
                var userId = _userManager.GetUserId(User);
                helperModel.ProviderId = userId;
                _context.Add(helperModel);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(helperModel);
        }

        // GET: Helper/Edit/5
        [Authorize(Roles = "Admin,Guarantor")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var helperModel = await _context.Helper.SingleOrDefaultAsync(m => m.HelperId == id);
            if (helperModel == null)
            {
                return NotFound();
            }
            return View(helperModel);
        }

        // POST: Helper/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin,Guarantor")]
        public async Task<IActionResult> Edit(int id, [Bind("HelperId,Surname,Firstname,Lastname,Gender,State,Religion,Qualification," +
            "Language,Status,DateOfBirth,NextOfKinName,NextOfKinAddress,NextOfKinPhone,ProviderId,CreatedAt,UpdatedAt")] HelperModel helperModel)
        {
            if (id != helperModel.HelperId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(helperModel);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HelperModelExists(helperModel.HelperId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(helperModel);
        }

        // GET: Helper/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var helperModel = await _context.Helper
                .SingleOrDefaultAsync(m => m.HelperId == id);
            if (helperModel == null)
            {
                return NotFound();
            }

            return View(helperModel);
        }

        // POST: Helper/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var helperModel = await _context.Helper.SingleOrDefaultAsync(m => m.HelperId == id);
            _context.Helper.Remove(helperModel);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HelperModelExists(int id)
        {
            return _context.Helper.Any(e => e.HelperId == id);
        }
    }
}
