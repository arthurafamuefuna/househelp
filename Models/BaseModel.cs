﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HouseHelp.Models
{
    public class BaseModel
    {
        public BaseModel()
        {
            CreatedAt = UpdatedAt = DateTime.Now;
        }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }
    }
}
