﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HouseHelp.Models
{
    public class EmployerModel:BaseModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EmployerId { get; set; }

        public string Surname { get; set; }

        public string Firstname { get; set; }
    
        public string Lastname { get; set; }

        public string Gender { get; set; }

        public string Religion { get; set; }

        public string Occupation { get; set; }

        public string Address { get; set; }

        public string State { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

    }
}
