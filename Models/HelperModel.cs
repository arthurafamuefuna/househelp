﻿using HouseHelp.AppUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HouseHelp.Models
{
    public class HelperModel:BaseModel
    {

        public HelperModel()
        {
            Status = HelperStatusEnum.Available;
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HelperId { get; set; }

        public string Surname { get; set; }

        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Gender { get; set; }

        public string State { get; set; }

        public string Religion { get; set; }

        public string Qualification { get; set; }

        public string Language { get; set; }

        public HelperStatusEnum Status { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public string DateOfBirth { get; set; }
        //public DateTime? DateOfBirth { get; set; }

        public string Employer { get; set; }

        public string NextOfKinName { get; set; }

        public string NextOfKinAddress { get; set; }

        public string NextOfKinPhone { get; set; }

        public string ProviderId { get; set; }
        
    }
}
